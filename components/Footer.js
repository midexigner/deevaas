import Link from "next/link";
import { FaFacebookF,FaInstagram,FaEnvelope } from 'react-icons/fa';

import styles from "../styles/footer.module.css";

const Footer = () => {
  return (
    <footer className={styles.footer}>
      <div className={`container ${styles.footerContainer}`}>
        <div className={styles.footerCols}>
          <h6>logo</h6>
          <ul className={styles.socialMedia}>
            <li><a href="#"><FaFacebookF/> </a></li>
            <li><a href="#"><FaInstagram/> </a></li>
            <li><a href="#"><FaEnvelope/> </a></li>
          </ul>
        </div>
        <div className={styles.footerCols}>
          <h6>Quick Links</h6>
          <ul className={styles.footerNav}>
            <li>
             <Link  href="/our-story"><a>Our Story</a></Link>
            </li>
            <li>
              <Link  href="/faqs"><a>FAQs</a></Link>
            </li>
            <li>
              <Link  href="/custom-order-form"><a>Ask For a Perfume</a></Link>
            </li>
            <li>
              <Link  href="/returns"><a>Ask For a Perfume</a></Link>
            </li>
            <li>
              <a href="experience-center1">Customer Experience Center</a>
            </li>
            <li>
              <a href="customized-giveaway">Bulk / Customize Orders</a>
            </li>
            <li>
              <a href="refund-policy">Refund Policy</a>
            </li>
            <li>
              <a href="terms-of-service">Terms of Service</a>
            </li>
          </ul>
        </div>
        <div className={styles.footerCols}>
          <h6>About Us</h6>
          <p>We sell high quality impressions of designer fragrances. We keep our costs low by having standard packaging and minimal advertising. This enables our customers to enjoy Ferrari on a Corolla budget.</p> 
          <p> <strong>Payment Methods:</strong></p> 
          <p>Cash On Delivery</p> 
          <p>Bank Transfer</p>
          
        </div>
        <div className={styles.footerCols}>
          <h6>Other</h6>
          <ul className={styles.footerNav}>
            <li>
             <Link  href="/our-story"><a>Our Story</a></Link>
            </li>
            <li>
              <Link  href="/faqs"><a>FAQs</a></Link>
            </li>
            <li>
              <Link  href="/custom-order-form"><a>Ask For a Perfume</a></Link>
            </li>
            <li>
              <Link  href="/returns"><a>Ask For a Perfume</a></Link>
            </li>
            <li>
              <a href="experience-center1">Customer Experience Center</a>
            </li>
            <li>
              <a href="customized-giveaway">Bulk / Customize Orders</a>
            </li>
            <li>
              <a href="refund-policy">Refund Policy</a>
            </li>
            <li>
              <a href="terms-of-service">Terms of Service</a>
            </li>
          </ul>
        </div>
        <div>
          <h6>Contact Us</h6>
          <p> <strong>Got a question? Get in touch.</strong></p> 
          <p>Phone: +92 xxx xxx xxxx</p> 
          <p>Address: Deevaas, Karachi, Pakistan</p>
        </div>
      </div>
      <div className="container">
      <p className={styles.credits}>
          &copy; {new Date().getFullYear()} <a href="/" title="">Deevaas</a>. All Right Reserved</p>
      </div>
    </footer>
  );
};

export default Footer;
