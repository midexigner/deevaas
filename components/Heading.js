import Link from 'next/link'
import styles from '../styles/heading.module.css'
const Heading = ({title,url}) => {
    return (
        <div className={styles.heading}>
        {url ?(<Link href={url}>{title}</Link>):(<h2>{title}</h2>)}
        </div>
    )
}

export default Heading
