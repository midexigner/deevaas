import styles from '../styles/Hero.module.css'
const Hero = ({url}) => {
    return (<section className={styles.Hero}>
        <img src={url} alt=""/>
    </section> );
}
 
export default Hero;