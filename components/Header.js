import Link from 'next/link'
import { useRouter } from 'next/router'
import styles from '../styles/header.module.css'
import { FaSearch } from 'react-icons/fa';
import { AiFillShop } from 'react-icons/ai';
import { BiCart } from 'react-icons/bi';

const Header = () => {
    return (
        <>
        <section className={styles.top_bar}>
<div className={`${styles.topContainer}`}>
    <div className={styles.topLeft}>CUSTOMER CARE: +92 xxx xxx xxxx</div>
    <div className={styles.topRight}>
    <div className={styles.topLoginMenu}><Link href="/login"><a>Login</a></Link></div>
    <div className={styles.topCartMenu}><Link href="/cart"><a><BiCart /><span>0</span></a></Link></div>
    </div>
</div>
        </section>
        <header className={styles.header}>
           <div className={`${styles.headerContainer}`}>
           <nav className={styles.navigation}>
               <ul>
                   <li className={styles.active}><Link href="/">Home</Link></li>
                   <li><Link href="/shop">Shop</Link></li>
                   <li><Link href="/faqs">FAQs</Link></li>
                   <li><Link href="/story">Our Story</Link></li>
                   <li><Link href="/blogs">Blogs</Link></li>
                   <li><Link href="/seller">Best Seller</Link></li>
               </ul>
               </nav>
            <div className={styles.logo}>
                <Link href="/"><a><span>
                <AiFillShop /><span>Deevaas</span>
                    </span></a></Link>
            </div>
            <div className={styles.rightSide}>
                    <div className={styles.search}>
                    <input type="search" placeholder={'Search'} />
                    <FaSearch />
                    </div>
            </div>
           </div>
        </header>
        </>
    )
}

export default Header
