import Heading from "./Heading"
import ProductCard from './ProductCard'
import styles from '../styles/OurBestSeller.module.css'

const OurBestSeller = () => {
    return (
        <section className={styles.ourBestSeller}>
         <div className="container"> <Heading title="Our best seller" /></div>
      <div className="styles.product__listing">
        <ProductCard />
      </div>
      </section>
    )
}

export default OurBestSeller
