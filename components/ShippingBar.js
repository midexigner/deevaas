import styles from '../styles/shippingBar.module.css'
const ShippingBar = ({title,price}) => {
    return (<section className={styles.shippingBar}>
        <div className="container">
            <p>{title}<span>{price}</span></p>
        </div>
    </section>);
}
 
export default ShippingBar;