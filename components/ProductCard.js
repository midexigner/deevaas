import Link from 'next/link'

const ProductCard = () => {
    return (
        <article className="product__card">
           <div className="product__inner"> 
            <div className="product__thumbnail">img here</div>
            <div className="product__info">ProductCard.js</div>
           </div>
              <style jsx>{`
        .product__card{
           position:relative;
        }
        .product__inner{
            position:relative;
        }
      `}</style>
        </article>
    )
}

export default ProductCard
