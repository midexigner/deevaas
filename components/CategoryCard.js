import Link from 'next/link'
import styles from '../styles/CategoryCard.module.css'

const CategoryCard = ({img,title,url}) => {
    return (
        <article className={styles.categoryItem}>
               <Link href={url}>
                <div  className={styles.categorytInner}>
                <div><img src={img} alt={title} /></div>
                 <div><h4>{title}</h4></div>
                </div>
               </Link>
               </article>
    )
}

export default CategoryCard
