import Footer from "./Footer";
import Header from "./Header";
import ShippingBar from "./ShippingBar";

const Layout = ({children}) => {
    return ( 
    <>
    <ShippingBar title="Free shipping for orders over" price="₨3000" />
     <Header />
     {children}
     <Footer />
    </> );
}
 
export default Layout;