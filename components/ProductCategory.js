import styles from '../styles/productCategory.module.css'
import CategoryCard from './CategoryCard'

const ProductCategory = () => {
    return (
        <div className={styles.productCategory}>
          <div className={`container ${styles.productCatContainer}`}>
             <CategoryCard 
             img={'https://images.pexels.com/photos/3613388/pexels-photo-3613388.jpeg'}
             title={`Men`}
             url={`/men`}
              />
             <CategoryCard 
             img={'https://images.pexels.com/photos/3750640/pexels-photo-3750640.jpeg'}
             title={`Women`}
             url={`/women`}
              />
             <CategoryCard 
             img={'https://images.pexels.com/photos/3989394/pexels-photo-3989394.jpeg'}
             title={`Eastern`}
             url={`/eastern`}
              />
             <CategoryCard 
             img={'https://images.pexels.com/photos/3613388/pexels-photo-3613388.jpeg'}
             title={`Western`}
             url={`/western`}
              />
          </div>
        </div>
    )
}

export default ProductCategory
