import Head from 'next/head'
import OurBestSeller from '../components/OurBestSeller'
import ProductCategory from '../components/ProductCategory'
import NewArrival from '../components/NewArrival'
import FlashSale from '../components/FlashSale'
import styles from '../styles/Home.module.css'
import Meta from '../components/Meta'
import Hero from '../components/Hero'

export default function Home() {
  return (
    <>
     <Meta/>
       <Hero url="https://images.pexels.com/photos/1051744/pexels-photo-1051744.jpeg"/>
      <ProductCategory />
      <OurBestSeller />
       <NewArrival />
      <FlashSale/>
      <ProductCategory />
      <h5>Varity of fit all</h5>
      <h5>3 cols img</h5>
      <h5>customer review</h5>
      <h5>customize section</h5>
      <h5>news section</h5>

    
    </>
  )
}
