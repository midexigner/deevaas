
import Layout from '../components/Layout.js'
import globalStyles from '../styles/globals.js'

function MyApp({ Component, pageProps }) {
  return (
  <Layout>
 
  <Component {...pageProps} />
  <style jsx global>
        {globalStyles}
      </style>
  </Layout>
    )
}

export default MyApp
